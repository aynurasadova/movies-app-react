import React, { useState } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.scss";
import "slick-carousel/slick/slick-theme.scss";
import Jumbotron from 'react-bootstrap/Jumbotron';

import "./scss/styles.scss";
import { NavLinks } from './NavLinks';
import { MovieDetails } from './MovieDetails';
import { Actions } from './Actions';

const settings = {
    dotsClass: "slick-dots",
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    fade: true,
    arrows: false
};

export const HomePage = () => {
    return (
        <Slider style = {{overflow: "hidden"}} {...settings}>
          <Jumbotron fluid className = "b1-image">
            <div className = "containerLinearGradient">
                <NavLinks />
                <MovieDetails
                    title = "Moonlight"
                    description = "This is the story of a lifetime"
                    details = "18+ | 1h 51min | Drama"
                    imdb = "7.4"
                />
                <Actions/>
            </div>
        </Jumbotron>
        <Jumbotron fluid className = "b2-image" >
            <div className = "containerLinearGradient">
                <NavLinks />
                <MovieDetails
                    title = "Joker"
                    description = "PUT ON A HAPPY FACE"
                    details = "18+ | 2h 2min | Crime / Drama / Thriller"
                    imdb = "8.6"
                />
                <Actions/>
            </div>
        </Jumbotron>
        <Jumbotron fluid className = "b3-image" >
            <div className = "containerLinearGradient">
                <NavLinks />
                <MovieDetails
                    title = "Once Upon a Time in ..."
                    description = "Hollywood"
                    details = "18+ | 2h 41min | Comedy / Drama "
                    imdb = "8.6"
                />
                <Actions/>
            </div>
        </Jumbotron>
        </Slider>
    )
}