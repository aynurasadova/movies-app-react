import React, { useState } from 'react';

import "./scss/styles.scss"
import Container from 'react-bootstrap/esm/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import { Navbar, Nav } from 'react-bootstrap';


export const NavLinks = () => {
    return (
        <Container fluid className = "navContainer">
            <Navbar expand = "lg" className = "bg-transparent links">
                <Navbar.Collapse className="justify-content-center " style = {{textAlign: "center"}}>
                    <Nav className="justify-content-center ">
                        <Nav.Item>
                            <Nav.Link className = "singleLink text-light" href = "/movies">Movies</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link className = "singleLink text-light" href = "/animations">Animations</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link className = "singleLink text-light" href = "/tvseries">TV series</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link className = "singleLink text-light" href = "/collections">Collections</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link className = "singleLink text-light" href = "/blogs">Blogs</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar> 
        </Container>
    )
}