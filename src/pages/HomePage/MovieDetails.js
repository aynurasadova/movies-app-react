import React, { useState } from 'react';

import "./scss/styles.scss"
import Container from 'react-bootstrap/esm/Container';


export const MovieDetails = ({title, description, details, imdb}) => {
    return (
        <Container fluid className = "detailsContainer">
            <p className = "titleOfMovie">
                {title}
            </p>
            <p className = "descOfMovie">
                {description}
            </p>
            <p className = "detailOfMovie">
                {details}
            </p>
            <p className = "imdbOfMovie">
                <span className = "imdb">
                    IMDb
                </span>
                {imdb}
            </p>
        </Container>
    )
}