// import React, { useEffect, useState, useCallback } from 'react';
// import { connect } from 'react-redux';

// import { getAllMoviesFromDB, selectAllMovies } from '../../redux/movies.reducer';
// import { Carousel } from 'react-bootstrap';
// import "./scss/styles.scss"

// const mapStateToProps = (state) => ({
//     allMovies: selectAllMovies(state),
// })

// export const MoviesPage = connect(mapStateToProps, {getAllMoviesFromDB})(({getAllMoviesFromDB, allMovies}) => {
    
//     // const [movies, setMovies] = useState(allMovies || []);
    
//     // useEffect(() => {
//     //     getAllMoviesFromDB();
//     // }, [movies]);

//     const [index, setIndex] = useState(0);

//     const handleSelect = (selectedIndex, e) => {
//       setIndex(selectedIndex);
//     };
  
//     return (
//         <div>
//             salam
//         </div>
//     //   <Carousel activeIndex={index} onSelect={handleSelect}>
//     //     <Carousel.Item>
//     //       <img
//     //         className="d-block w-100"
//     //         src="holder.js/800x400?text=First slide&bg=373940"
//     //         alt="First slide"
//     //       />
//     //       <Carousel.Caption>
//     //         <h3>First slide label</h3>
//     //         <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
//     //       </Carousel.Caption>
//     //     </Carousel.Item>
//     //     <Carousel.Item>
//     //       <img
//     //         className="d-block w-100"
//     //         src="holder.js/800x400?text=Second slide&bg=282c34"
//     //         alt="Second slide"
//     //       />
  
//     //       <Carousel.Caption>
//     //         <h3>Second slide label</h3>
//     //         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
//     //       </Carousel.Caption>
//     //     </Carousel.Item>
//     //     <Carousel.Item>
//     //       <img
//     //         className="d-block w-100"
//     //         src="holder.js/800x400?text=Third slide&bg=20232a"
//     //         alt="Third slide"
//     //       />
  
//     //       <Carousel.Caption>
//     //         <h3>Third slide label</h3>
//     //         <p>
//     //           Praesent commodo cursus magna, vel scelerisque nisl consectetur.
//     //         </p>
//     //       </Carousel.Caption>
//     //     </Carousel.Item>
//     //   </Carousel>
//     );
// })

import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.scss";
import "slick-carousel/slick/slick-theme.scss";

export class MoviesPage extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false
    };
    return (
      <div>
        
        <Slider {...settings}>
          <div>
            <h3>1</h3>
          </div>
          <div>
            <h3>2</h3>
          </div>
          <div>
            <h3>3</h3>
          </div>
          <div>
            <h3>4</h3>
          </div>
          <div>
            <h3>5</h3>
          </div>
          <div>
            <h3>6</h3>
          </div>
        </Slider>
      </div>
    );
  }
}