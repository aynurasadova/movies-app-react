import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';

import "./scss/styles.scss";
import { useWindowDimensions } from '../utils/useWindowDimensions';
import {Navbar, Nav, Container} from "react-bootstrap"
import { MoviesPage } from '../pages/MoviesPage';
import { HomePage } from '../pages/HomePage';

export const RootNav = () => {
    const {width} = useWindowDimensions();

    const changeLinksStyle = width <= 921 ? true : false;
    return(
        <>
        <Container fluid className = "rootNavContainer">
        <Navbar className = "nav-bar logoWrapper">
            <Nav.Item>
                <Nav.Link className = "logo" href = "/" >LOGO</Nav.Link>
            </Nav.Item>
        </Navbar>
        </Container>

        <Switch>
            <Route exact path = "/" component = {HomePage} />
            <Route exact path = "/movies" component = {MoviesPage} />
        </Switch>
        </>
    )
}